package main

import (
	"fmt"

	"gitlab.com/alexia.shaowei/swredis"
)

var cache *swredis.SWRedis
var errCH error

func init() {
	cache, errCH = swredis.New("127.0.0.1:6379", "")
	if errCH != nil {
		fmt.Printf("errCH: %v\n", errCH)
	}
	if cache == nil {
		fmt.Println("cache pointer nil")
	}
}

func main() {
	fmt.Println("main::main()")

	// resp, err := cache.HGetAll("123456")
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// val, ok := interface{}(resp).(map[string]string)
	// if ok {
	// 	fmt.Println(val)
	// }

	// cache.HSet("hashk", "f1", "v1")
	// cache.HSet("hashk", "f2", "v1")
	// cache.HSet("hashk", "f3", "v1")
	// cache.HSet("hashk", "f4", "v1")
	// cache.HSet("hashk", "f5", "v1")
	// resp, _ := cache.HGetAll("hashk")

	// m := map[string]interface{}{"A": "av", "B": "bv", "C": 1, "D": 1, "E": 1}
	// time.Sleep(time.Second * 20)

	fmt.Println(cache.Ping())

}
