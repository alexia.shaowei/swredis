package swredis

import (
	"context"
	"time"

	"github.com/go-redis/redis/v8"
)

func New(redishost string, site string) (*SWRedis, error) {

	ctx = context.Background()

	client := redis.NewClient(&redis.Options{
		Addr:        redishost,
		PoolSize:    10,
		PoolTimeout: 30 * time.Second,
	})

	if _, err := client.Ping(ctx).Result(); err != nil {
		return nil, err
	}

	return &SWRedis{
		rdsClient: client,
	}, nil
}
