package swredis

import (
	"encoding/json"
	"time"
)

func formatData(data string, expire int32) string {

	now := uint32(time.Now().Unix())

	var redisData RedisData = RedisData{
		Data:    data,
		Expire:  uint32(expire) + now,
		AddTime: now,
	}

	jsn, err := json.Marshal(redisData)
	if err != nil {
		return ""
	}

	return string(jsn)
}

func extractData(jsonStr string) *RedisData {

	var data *RedisData

	json.Unmarshal([]byte(jsonStr), &data)

	return data
}
